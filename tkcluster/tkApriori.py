def loadTKDataset():
    D=[]
    with open('/home/farhan/Desktop/tkordersdata.basket') as f:
        for line in f:
            line=line[:-1]
            items=line.split(',')
            oneSet=set()
            for item in items:
                oneSet.add(item)
            D.append(oneSet)
    return D

def load_dataset():
    "Load the sample dataset."

    D=[]
    D.append(set([1, 2, 5]))
    D.append(set([2, 4]))
    D.append(set([2, 3]))
    D.append(set ([1, 2, 4]))
    D.append( set([1,3]))
    D.append( set([2,3]))
    D.append( set([1,3]))
    D.append( set([1,2,3,5]))
    D.append( set([1,2,3]))
    return D



def createC1(dataset):
    "Create a list of candidate item sets of size one."
    c1 = []
    for transaction in dataset:
        for item in transaction:
            item=frozenset([item])
            if item not in c1:
                c1.append(frozenset(item))
    #frozenset because it will be a ket of a dictionary.
    return c1


def scanD(dataset, candidates, min_support):
    "Returns all candidates that meets a minimum support level"
    sscnt = {}
    for can in candidates:
        sscnt[can]=0
    for tid in dataset:
        for can in candidates:
            if can.issubset(tid):
                sscnt[can] += 1

    num_items = float(len(dataset))
    retlist = []
    pruned=[]
    support_data = {}
    for key in sscnt:
        support = sscnt[key] / num_items
        if support >= min_support:
            retlist.insert(0, key)
        else:
            pruned.insert(0,key)
        support_data[key] = support
    return retlist, pruned,support_data

def generateSuperset(freq_set,length):
    L2=[]
    for i in range (len(freq_set)):
        for j in range(i + 1, len(freq_set)):
            super=freq_set[i]|freq_set[j]
            if(len(super)==length and super not in L2):
                L2.append(super)

    return L2

def pruneDataSet(pruneSet,L):
    for item in L:
        for j in pruneSet:
            if j.issubset(L):
                L.remove(item)
    return L

def supportMapper(support_map,suppport_data):
    for key in support_data:
        support_map[key]=support_data[key]
    return support_map

L=[]
freqList=[]
C=[]
pruned=[]
support_map={}
dataset=loadTKDataset()
#print(dataset)
C.append(createC1(dataset))
print( 'C: ',C[0])

supported,rejected,support_data=scanD(dataset,C[0],0.00001)
L.append(supported)
pruned.append(rejected)
print('L: ',L[0])


print('length of diff comb',len(L[0]))
print('pruned :',pruned[0])
support_map=supportMapper(support_map,support_data)
print(support_data)
count=2
for i in range(1,len(L[0])):

    C.append(generateSuperset(L[i-1],count))
    print('C: ',C[i])

    supported,rejected,support_data=scanD(dataset,C[i],0.00001)
    pruned.append(rejected)
    if len(supported)==0 or len(supported)==1:
        break
    print('L: ',supported)
    print('pruned: ',pruned[i])
    L.append(pruneDataSet(pruned[i-1],supported))
    print('new L: ',L[i])
    print(support_data)
    support_map=supportMapper(support_map,support_data)

    count+=1
    #print('\n')

print('-----------------------------------------------')
for itemList in L:
    for item in itemList:
        print(item)
for key in support_map:
    print(str(key)+'------>'+str('%.10f'%support_map[key]))